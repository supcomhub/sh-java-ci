FROM ubuntu:18.04

ENV DOCKER_COMPOSE_VERSION=1.23.2

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
      apt-transport-https \
      ca-certificates \
      curl \
      gpg-agent \
      software-properties-common \
      git \
  && rm -rf /var/lib/apt/lists/*

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
  && add-apt-repository \
      "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) \
      stable" \
  && apt-get install -y --no-install-recommends docker-ce \
  && curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir /java \
    && curl https://download.java.net/java/GA/jdk10/10.0.2/19aef61b38124481863b1413dce1855f/13/openjdk-10.0.2_linux-x64_bin.tar.gz \
      | tar xz --strip-components=1 --directory /java

ENV JAVA_HOME=/java
ENV PATH="${JAVA_HOME}/bin:${PATH}"

